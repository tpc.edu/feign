# SpringBoot集成feign

## 要点
> 重点理解`feign.Feign.Builder`中各项属性(`Client` `Contract` `Encoder` `Decoder` `RequestInterceptor`)的触发时机、作用域、主要功能及实战拓展。  
> 缺省配置`org.springframework.cloud.openfeign.FeignClientsConfiguration`  
> FeignClient初始化`org.springframework.cloud.openfeign.FeignClientFactoryBean.getObject`
* [feign基础使用](docs/demo.md)
* [feign自定义配置](docs/config.md)
* [feign设置Header](docs/header.md)
* [feign调用上传文件](docs/UploadFile.md)
* [feign解码器](https://cloud.tencent.com/developer/article/1588501 "原生Feign的解码器Decoder、ErrorDecoder")
* [feign熔断支持](docs/Hystrix.md)
* [feign异常处理](docs/ErrorDecoder.md)
* [feign环绕通知](docs/client.md)
* [feign调用OAuth2授权服务](docs/OAuth2.md)
* [feign日志配置](docs/log.md)
* [feign负载均衡策略](docs/LoadBalancer.md)

## 参考
* [官方文档](https://spring.io/projects/spring-cloud-openfeign) [^1][^2]
* [中文文档](https://segmentfault.com/a/1190000018914017 "Spring Cloud 参考文档（声明式REST客户端：Feign）")
* [中文文档](https://segmentfault.com/a/1190000018313243 "翻译: Spring Cloud Feign使用文档")
* [用法参考](https://blog.csdn.net/fox_bert/article/details/98987776 "Spring Cloud(三) Spring Cloud OpenFeign （服务调用组件，熔断降级，负载均衡）")
* [源码-feign-demo](https://gitlab.com/tpc.edu/feign)
* [源码-feign-learning](https://github.com/yourbatman/feign-learning)

[^1]: [3.0.1](https://docs.spring.io/spring-cloud-openfeign/docs/current/reference/html/)
[^2]: [3.0.0-SNAPSHOT](https://cloud.spring.io/spring-cloud-openfeign/reference/html/#spring-cloud-feign-inheritance)