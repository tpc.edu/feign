[TOC]

# 重写OpenFeign的ErrorDecoder获取服务提供方抛出的原始异常信息

## 概述

在构建微服务的过程中, 我们时常会需要借助 Spring Cloud Open Feign 组件调用第三方依赖服务. 当服务提供方响应为非 2xx 状态码时, feign调用将会抛出`FeignException`. 由于其异常`message`经过了`Feint`的封装, 所以不再是服务提供方的原始异常信息. 若想展示原始信息则需要重写`ErrorDecoder`来实现, 下面介绍重写的两种方式:

* 重写`ErrorDecoder`
* 重写`ErrorDecoder.Default`

## 源码分析

通过对`AsyncResponseHandler#handleResponse()`及`feign.FeignException#errorStatus(java.lang.String, feign.Response)`可以得知: `feingClient.method`方法返回值类型不是`feign.Response`, 响应状态码为非 2xx(配置了404状态解码时需排除404)时, 将会抛出`FeignException`. 由于其对服务调用方的原始信息做了加工, 所以需要重写`ErrorDecoder`才能得到抛出的原始异常信息.
```java
void handleResponse(CompletableFuture<Object> resultFuture,
                      String configKey,
                      Response response,
                      Type returnType,
                      long elapsedTime) {
    // copied fairly liberally from SynchronousMethodHandler
    boolean shouldClose = true;

    try {
      if (logLevel != Level.NONE) {
        response = logger.logAndRebufferResponse(configKey, logLevel, response,
            elapsedTime);
      }
      if (Response.class == returnType) {
        if (response.body() == null) {
          resultFuture.complete(response);
        } else if (response.body().length() == null
            || response.body().length() > MAX_RESPONSE_BUFFER_SIZE) {
          shouldClose = false;
          resultFuture.complete(response);
        } else {
          // Ensure the response body is disconnected
          final byte[] bodyData = Util.toByteArray(response.body().asInputStream());
          resultFuture.complete(response.toBuilder().body(bodyData).build());
        }
      } else if (response.status() >= 200 && response.status() < 300) {
        if (isVoidType(returnType)) {
          resultFuture.complete(null);
        } else {
          final Object result = decode(response, returnType);
          shouldClose = closeAfterDecode;
          resultFuture.complete(result);
        }
      } else if (decode404 && response.status() == 404 && !isVoidType(returnType)) {
        final Object result = decode(response, returnType);
        shouldClose = closeAfterDecode;
        resultFuture.complete(result);
      } else {
        resultFuture.completeExceptionally(errorDecoder.decode(configKey, response));
      }
    } catch (final IOException e) {
      if (logLevel != Level.NONE) {
        logger.logIOException(configKey, logLevel, e, elapsedTime);
      }
      resultFuture.completeExceptionally(errorReading(response.request(), response, e));
    } catch (final Exception e) {
      resultFuture.completeExceptionally(e);
    } finally {
      if (shouldClose) {
        ensureClosed(response.body());
      }
    }

  }
```

## 保留服务提供方抛出的原始异常信息

OpenFeign自定义配置
```java
@Configuration
public class KeepErrMsgConfiguration {
    @Bean
    public ErrorDecoder errorDecoder() {
        return new RawErrorDecoder();
    }
}
```

### 重写`ErrorDecoder`

``` java
public class RawErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        String message = null;
        try {
            if (response.body() != null) {
                message = Util.toString(response.body().asReader(Util.UTF_8));
                JSONObject json = JSONObject.parseObject(message);
                return new RuntimeException(json.getString("message"));
            }
        } catch (Exception ignored) {
        }
        return new RuntimeException(message);
    }
}
```

### 重写`ErrorDecoder.Default`

``` java 
public class RawErrorDecoder extends ErrorDecoder.Default {
    @Override
    public Exception decode(String methodKey, Response response) {
        Exception exception = super.decode(methodKey, response);
        if (exception instanceof RetryableException) {
            return exception;
        }
        try {
            if (response.body() != null) {
                String message = Util.toString(response.body().asReader(Util.UTF_8));
                return new RuntimeException(message);
            }
        } catch (IOException ignored) {
        }
        return exception;
    }
}
```

## 参考

* [优秀文档](https://juejin.cn/post/6844903620018700302 "Spring Cloud Feign 熔断机制填坑") 
* [参考文档](https://blog.csdn.net/zhaokejin521/article/details/106908899 "Feign调用全局异常处理解决")
* [参考文档](https://blog.csdn.net/xieedeheishou/article/details/107094469 "[Feign] 如何处理非 2xx 的响应? 如何获取 Header ?") 
* [本文源码](https://gitlab.com/tpc.edu/feign)
