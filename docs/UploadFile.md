# SpringCloud openfeign 调用文件上传服务

## 概述
如何使用`openfeign`调用文件上传的远程服务呢? 其`feignClient`如何编写呢? 

* 使用`@RequestPart`

## 示例说明

`FeignClient`接口中文件类型的入参要用`@RequestPart`注解, 且要设置请求头信息`ContentType`为`multipart/form-data`.

### 编写feignClient

``` java 
@PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE )
void upload(@RequestPart(value = "file") MultipartFile file);
```

## 参考
* [参考文档-Spring Cloud Feign Client 上传文件](https://blog.csdn.net/qq_32786873/article/details/79756720)
* [源码-feign-demo](https://gitlab.com/tpc.edu/feign)
