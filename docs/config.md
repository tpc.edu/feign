# SpringBoot-feign之自定义配置

## 概述
使用`Feign`默认配置可能不能满足需求，这时就需要我们实现自己的`Feign`配置，配置方式:
* `application.properties(.yml)`全局和局部(针对单个Feign接口)
* spring `java config`全局配置和局部(针对单个Feign接口)

## 示例说明
具体配置项如下，如何配置可以参考`FeignClientsConfiguration`或`Feign.Builder`类：
`Logger.Level`：日志级别
`Retryer`: 重试机制
`ErrorDecoder`：错误解码器
`Request.Options`:
`RequestInterceptor`：请求拦截器
`Contract`:处理Feign接口注解，Spring Cloud Feign 使用`SpringMvcContract` 实现，处理Spring mvc 注解，也就是我们为什么可以用Spring mvc 注解的原因。
`Client`：Http客户端接口，默认是`Client.Default`，但是我们是不使用它的默认实现，Spring Cloud Feign为我们提供了`okhttp3`和`ApacheHttpClient`两种实现方式，只需使用maven引入以下两个中的一个依赖即可,版本自由选择。
`Encoder`：编码器，将一个对象转换成http请求体中， Spring Cloud Feign 使用 `SpringEncoder`
`Decoder`：解码器， 将一个http响应转换成一个对象，Spring Cloud Feign 使用 `ResponseEntityDecoder`
`FeignLoggerFactory`：日志工厂参考[Spring Cloud Feign 之日志自定义扩展](https://www.jianshu.com/p/ae369d38c8b2)
`Feign.Builder`：`Feign`接口构建类，覆盖默认`Feign.Builder`，比如：`HystrixFeign.Builder`
`FeignContext`管理了所有的java config 配置

* 方式一: `application.properties(.yml)`局部(针对单个Feign接口)
```yaml
feign:
  client:
    config:
      feignName:
        connectTimeout: 5000
        readTimeout: 5000
        loggerLevel: full
        errorDecoder: com.example.SimpleErrorDecoder
        retryer: com.example.SimpleRetryer
        requestInterceptors:
          - com.example.FooRequestInterceptor
          - com.example.BarRequestInterceptor
        decode404: false
        encoder: com.example.SimpleEncoder
        decoder: com.example.SimpleDecoder
        contract: com.example.SimpleContract
        client: com.example.SimpleClient
```
* 方式二: `application.properties(.yml)`全局(针对所有Feign接口)
```yaml
feign:
  client:
    config:
      default:
        connectTimeout: 5000
        readTimeout: 5000
        loggerLevel: basic
        # ... (该处省略,属性同局部设置)
```
* 方式三: spring `java config`局部(针对单个Feign接口)
```java 
@Configuration
public class FooConfiguration {
    @Bean
    public Contract feignContract() {
        return new feign.Contract.Default();
    }

    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("user", "password");
    }
}
```
```java
@FeignClient(name = "user", url = "${user.url}", configuration = UserFeignClientConfig.class)
public interface UserFeign {
    @GetMapping("/{id}")
    User getUserByID(@PathVariable("id") String id);
}
```
* 方式四: spring `java config`全局
```java
@EnableFeignClients(defaultConfiguration = FeignClientsConfig.class)
@SpringBootApplication
public class FeignApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeignApplication.class, args);
    }
}
```

## 自定义配置优先级
下面代码就是处理配置使之生效,`FeignClientFactoryBean#configureFeign` :
```java
protected void configureFeign(FeignContext context, Feign.Builder builder) {
	FeignClientProperties properties = applicationContext.getBean(FeignClientProperties.class);

	FeignClientConfigurer feignClientConfigurer = getOptional(context, FeignClientConfigurer.class);
	setInheritParentContext(feignClientConfigurer.inheritParentConfiguration());

	if (properties != null && inheritParentContext) {
		if (properties.isDefaultToProperties()) {
			configureUsingConfiguration(context, builder);
			configureUsingProperties(properties.getConfig().get(properties.getDefaultConfig()), builder);
			configureUsingProperties(properties.getConfig().get(contextId), builder);
		}
		else {
			configureUsingProperties(properties.getConfig().get(properties.getDefaultConfig()), builder);
			configureUsingProperties(properties.getConfig().get(contextId), builder);
			configureUsingConfiguration(context, builder);
		}
	}
	else {
		configureUsingConfiguration(context, builder);
	}
}
```

### 第一种：配置文件无配置
使用 `java config` 配置，优先级有低到高进行单个配置覆盖
1. `FeignClientsConfiguration` Spring Cloud Feign 全局默认配置。
2. `@EnableFeignClients#defaultConfiguration` 自定义全局默认配置。
3. `FeignClient#configuration` 单个`Feign`接口局部配置。

### 第二种：feign.client.default-to-properties=true(默认true)
`java config` 和`application.properties(.yml)`配置，优先级有低到高进行单个配置覆盖
1. `FeignClientsConfiguration` Spring Cloud Feign 全局默认配置。
2. `@EnableFeignClients#defaultConfiguration` 自定义全局默认配置。
3. `FeignClient#configuration` 单个`Feign`接口局部配置。
4. `application.properties(.yml)`配置文件全局默认配置，配置属性`feign.client.default-config`指定默认值(defult)
5. `application.properties(.yml)`配置文件局部配置，指定`@FeignClient#name`局部配置。

### 第三种：feign.client.default-to-properties=false(默认true)
`java config` 和`application.properties(.yml)`配置，优先级有低到高进行单个配置覆盖
1. `application.properties(.yml)`配置文件全局默认配置，配置属性`feign.client.default-config`指定默认值(defult)
2. `application.properties(.yml)`配置文件局部配置，指定`@FeignClient#name`局部配置。
3. `FeignClientsConfiguration` Spring Cloud Feign 全局默认配置。
4. `@EnableFeignClients#defaultConfiguration` 自定义全局默认配置。
5. `FeignClient#configuration` 单个`Feign`接口局部配置。

## 参考
* [中文文档](https://segmentfault.com/a/1190000018914017 "Spring Cloud 参考文档（声明式REST客户端：Feign）")
* [参考文档](https://blog.csdn.net/sun_shaoping/article/details/82078717?utm_term=feignclient%E8%87%AA%E5%AE%9A%E4%B9%89%E9%85%8D%E7%BD%AE%E7%B1%BB&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-1-82078717&spm=3001.4430 "Spring Cloud Feign(第六篇) 之自定义配置")
* [源码-feign-demo](https://gitlab.com/tpc.edu/feign)
