# SpringBoot集成feign

## 核心
* 引入jar: `spring-cloud-starter-openfeign`
* 配置yml: `feign.service.url: https://www.baidu.com/doc`
* 启动feign-client: `@EnableFeignClients`
* 编写feign服务: FeignService.java
* 调用feign服务

## 示例代码

### 在`pom.xml`添加`feign`依赖
```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
    <version>3.0.0</version>
</dependency>
```

### 在`application.yml`添加URL
```yaml
weather.api.url: https://tianqiapi.com
```

### 在启动类添加`@EnableFeignClients`
```java
@EnableFeignClients
@SpringBootApplication
public class FeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeignApplication.class, args);
    }

}
```

### 编写feign服务
```java
@FeignClient(url = "${weather.api.url}", name = "weather")
public interface WeatherService {

    /**
     * 查询指定城市天气状态
     * @param appId     AppId
     * @param appSecret AppSecret
     * @return 天气状况
     */
    @GetMapping("/api?version=v6&appid={appId}")
    JSONObject getEngineMessage(@PathVariable("appId") String appId, @RequestParam("appsecret") String appSecret);

}
```

### 调用feign服务

```java
@Service
public class FeignService {

  @Resource
  private WeatherService weatherService;

  public JSONObject getWeatherMessage() {
    return weatherService.getEngineMessage("appId", "appSecret");
  }

}
```

## 参考

* [参考文档-SpringBoot访问接口的三种方式](https://www.cnblogs.com/pengdai/p/11764243.html)
* [参考文档-免费天气API](https://www.tianqiapi.com/index/doc?version=v6)
* [源码-feign-demo](https://gitlab.com/tpc.edu/feign)
