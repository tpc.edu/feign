# 设置Feign的Header信息

## 概述
在微服务间使用Feign进行远程调用时需要在 header 中添加信息，那么 springcloud open feign 如何设置 header 呢？有5种方式可以设置请求头信息: 

* 在`@RequestMapping`注解里添加headers属性
* 在方法参数前面添加`@RequestHeader`注解
* 在方法或者类上添加`@Headers`的注解
* 在方法参数前面添加`@HeaderMap`注解
* 实现`RequestInterceptor`接口

## 示例说明

由于Feign是完全支持Spring MVC注解的, 所以推荐使用前两种Feign设置header的方式, 即: Spring MVC中使用注解设置header.

### 在`@RequestMapping`注解里添加headers属性

在application.yml中配置

``` yaml
app.secret: appSecretVal
```
编写feignClient

``` java 
@PostMapping(value = "/book/api", headers = {"Content-Type=application/json;charset=UTF-8", "App-Secret=${app.secret}"})
void saveBook(@RequestBody BookDto condition);
```

### 在方法参数前面添加`@RequestHeader`注解

* 设置单个header属性
``` java 
@GetMapping(value = "/getStuDetail")
public StudentVo getStudentDetail(@RequestBody StudentDto condition, @RequestHeader("Authorization") String token);
```
* 设置多个header属性

``` java
@PostMapping(value = "/card")
public CardVo createCard(@RequestBody CardDto condition, @RequestHeader MultiValueMap<String, String> headers);
```

> 查看源码 org.springframework.web.bind.annotation.RequestHeader 说明:
> If the method parameter is Map<String, String>, MultiValueMap<String, String>, or HttpHeaders then the map is populated with all header names and values.

### 在方法或者类上添加`@Headers`的注解

#### 使用feign自带契约
``` java
@Configuration
public class FooConfiguration {
    @Bean
    public Contract feignContract() {
        return new feign.Contract.Default();
    }
}
```

FeignClient使用`@RequestLine`注解, 而未配置feign自带契约`Contract`时, `@Headers`不会起作用, 而且启动项目会报错: 
``` java
Method xxx not annotated with HTTP method type (ex. GET, POST)
```
> 解决该问题方案请参考[博客](https://blog.csdn.net/liangweihua123/article/details/87881358)

查阅官方文档，feign 默认使用的是spring mvc 注解（就是RequestMapping 之类的） ，所以需要通过新增一个配置类来修改其契约 ，即可可以解决该问题了。
``` java
@RequestLine is a core Feign annotation, but you are using the Spring Cloud @FeignClientwhich uses Spring MVC annotations. 
```

#### 配置`@Headers`注解
``` java 
@FeignClient(url = "${user.api.url}", name = "user", configuration = FooConfiguration.class)
public interface UserFeignClient {
    @RequestLine("GET /simple/{id}")
    @Headers({"Content-Type: application/json;charset=UTF-8", "Authorization: {token}"})
    public User findById(@Param("id") String id, @Param("token") String token);
}
```
使用`@Param`可以动态配置Header属性
> 网上很多在说 `@Headers`不起作用，其实`@Headers`注解没有生效的原因是：官方的`Contract`没有生效，想了解的移步这篇[博客](https://my.oschina.net/icebergxty/blog/3081099)

### 在方法参数前面添加`@HeaderMap`注解

#### 使用feign自带契约
同上

#### 配置`@HeaderMap`注解
``` java 
@FeignClient(url = "${user.api.url}", name = "user", configuration = FooConfiguration.class)
public interface UserFeignClient {
    @RequestLine("GET /simple/{id}")
    public User findById(@Param("id") String id, @HeaderMap HttpHeaders headers);
}
```

### 实现`RequestInterceptor`接口

值得注意的一点是:
如果`FeignRequestInterceptor`注入到spring容器的话就会全局生效, 就是说即使在没有指定`configuration`属性的`FeignClient`该配置也会生效, 为什么呢?有兴趣的请看[源码分析](http://www.suoniao.com/article/5f87f9066e70000040000126).
配置`@Component`或`@Service`或`@Configuration`就可以将该配置注入spring容器中, 即可实现全局配置, 从而该项目中的所有`FeignClient`的feign接口都可以使用该配置.
如果只想给指定`FeignClient`的feign接口使用该配置, 请勿将该类配置注入spring中.

``` java 
@Configuration
public class FeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        template.header(HttpHeaders.AUTHORIZATION, "tokenVal");
    }

}
```
> 小疑问: 如何在`RequestTemplate template`对象中[获取feign接口的请求体数据](http://www.suoniao.com/article/5f87f9066e70000040000126)呢?

``` java 
@FeignClient(url = "${user.api.url}", name = "user", configuration = FeignRequestInterceptor.class)
public interface UserFeignClient {
    @GetMapping(value = "/simple/{id}")
	public User findById(@RequestParam("id") String id);
}
```

## 参考
* [参考文档-feign设置header](https://blog.csdn.net/hkk666123/article/details/113964715)[^1][^2][^3]
* [源码-feign-demo](https://gitlab.com/tpc.edu/feign)

[^1]: [Spring 使用 feign时设置header信息](https://www.cnblogs.com/hui-run/p/8969702.html)
[^2]: [使用Feign时如何设置Feign的Header信息](https://my.oschina.net/aulbrother/blog/1610011)
[^3]: [设置Feign的Header信息](https://www.cnblogs.com/doagain/p/11087925.html)