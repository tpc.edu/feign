[TOC]

# Feign 失败降级未生效和超时配置优先级问题

## 问题

**服务A** 通过`feign`调用 **服务B**，服务A报警信息如下：

![error](https://img2018.cnblogs.com/blog/733995/201911/733995-20191120101737314-723017270.png)

经过详细分析后发现该报警由下面原因造成
1. 服务A调用服务B失败，未触发声明的失败降级操作
2. 同时配置`ribbon`和`feign`超时时间，优先级问题
```yaml
feign:
  client:
    config:
      pay-service: # 对服务提供者（优先级高）：填对应服务提供者名称，
                   # 对所有提供者(优先级低)：固定"default"
        connectTimeout: 3000 # 连接超时时间单位ms
        readTimeout: 8000  # 读取超时时间单位ms
ribbon:
  ReadTimeout: 60000 #ribbon连接超时
  ConnectTimeout: 60000 #ribbon读取超时
```

## 解决

针对上述问题对源码进行debug分析得出如下结论:

1. `Feign`降级生效配置
```yaml
feign.hystrix.enabled: true
```

2. 超时时间(`feign`的配置优先级高于`ribbon`)
**同时配置`ribbon`和`feign`，`feign`会覆盖`ribbon`，详见类`LoadBalancerFeignClient`的如下方法**
```java
@Override
public Response execute(Request request, Request.Options options) throws IOException {
   try {
      URI asUri = URI.create(request.url());
      String clientName = asUri.getHost();
      URI uriWithoutHost = cleanUrl(request.url(), clientName);
      FeignLoadBalancer.RibbonRequest ribbonRequest = new FeignLoadBalancer.RibbonRequest(
            this.delegate, request, uriWithoutHost);

      IClientConfig requestConfig = getClientConfig(options, clientName);
      return lbClient(clientName).executeWithLoadBalancer(ribbonRequest,
            requestConfig).toResponse();
   }
   catch (ClientException e) {
      IOException io = findIOException(e);
      if (io != null) {
         throw io;
      }
      throw new RuntimeException(e);
   }
}
```
第10行使用`feign`的超时时间配置(`Request.Options`)构造`IClientConfig`

第11行`lbClient(clientName)`构造`RetryableFeignLoadBalancer`对象，然后在`executeWithLoadBalancer`方法中会重新注入上一步生成的`IClientConfig` 

所以`feign`会覆盖`ribbon`的配置，优先级更高

## 源码分析
知其然并知其所以然，`Feign`的主要流程（重点类和重要方法）总结如下：
![feign-code](https://img2018.cnblogs.com/blog/733995/201911/733995-20191120153521119-1521073456.png)

[参考文档](https://www.cnblogs.com/mxmbk/p/11895963.html "Feign 失败降级未生效和超时配置优先级问题")