# SpringBoot集成feign配置全局log

## 概述
> 项目里使用了Feign进行远程调用，有时为了问题排查，需要开启请求和响应日志，下面简介一下如何开启Feign日志

## 设置Feign接口日志级别为FULL

### 在`application.yml`添加`log`配置

* 方式一: 在启动类路径设置debug级别的日志(**推荐**)
```yaml 
logging:
  level:
    com.example.demo: debug 
```
* 方式二: 在配置@FeignClien的类径设置debug级别的日志
``` yaml 
logging:
  level:
    com.example.demo.feign.DemoFeignClient: debug  
```

### 配置`feign`的日志级别

* 方式一: 在`application.yml`全局配置(**推荐**)
``` yaml
feign.client.config.default.loggerLevel: full
```
* 方式二: 在`application.yml`局部配置(**推荐**)
``` yaml
feign.client.config.feignName.loggerLevel: full
```
* 方式三: 使用`@Configuration`配置类完成配置
``` java 
@Configuration
public class FeignConfiguration {
  @Bean
  public Logger.Level feignLoggerLevel() { return Logger.Level.FULL; }
}
```
> Feign日志级别: 
> *  NONE，无记录（DEFAULT）。
> *  BASIC，只记录请求方法和URL以及响应状态代码和执行时间。
> *  HEADERS，记录基本信息以及请求和响应标头。
> *  FULL，记录请求和响应的头文件，正文和元数据

## 参考
* [参考文档-feign配置log](https://blog.csdn.net/youbl/article/details/109047987)[^1]
* [源码-feign-demo](https://gitlab.com/tpc.edu/feign)

[^1]: [spring cloud中将feign调用的请求header、返回header(详细日志)打印出来](https://blog.csdn.net/z710757293/article/details/109519831)