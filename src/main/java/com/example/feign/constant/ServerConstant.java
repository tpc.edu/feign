package com.example.feign.constant;

/**
 * @author LiuDong
 */
public enum ServerConstant {

    /**
     * code, reasonPhrase
     */
    INVALID_TOKEN("401", "Unauthorized");

    private final String code;

    private final String reasonPhrase;

    ServerConstant(String code, String reasonPhrase) {
        this.code = code;
        this.reasonPhrase = reasonPhrase;
    }

    public String getCode() {
        return code;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }
}
