package com.example.feign.feign.client;

import com.example.feign.feign.dto.CardDto;
import com.example.feign.feign.vo.CardVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author LiuDong
 * @date 2022/03/02 15:31
 * @description SpringCloud openfeign 同时包含文件上传及RequestBody的 feignClient
 */
@FeignClient(url = "${file.api.url}", name = "file")
public interface FileFeignClient {

    /**
     * 创建卡号
     * @param condition 入参
     * @return 卡号
     */
    @PostMapping(value = "/card")
    CardVo create(@RequestBody CardDto condition);

    /**
     * 文件上传服务
     * @param file 文件
     * @param name 名字
     * @return 文件信息
     */
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE )
    CardVo upload(@RequestPart(value = "file") MultipartFile file, @RequestParam("name") String name);

}
