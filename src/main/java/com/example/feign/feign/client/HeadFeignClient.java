package com.example.feign.feign.client;

import com.example.feign.feign.dto.CardDto;
import com.example.feign.feign.vo.CardVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author LiuDong
 * desc FeignClient设置Header的多种方式
 * See feign.SynchronousMethodHandler#invoke(java.lang.Object[])
 */
@FeignClient(url = "${virtual.api.url}", name = "head")
public interface HeadFeignClient {

    /**
     * 获取卡号
     * desc OpenFeign @QueryMap注解为POJO提供了支持，可用作GET参数映射，不幸的是，OpenFeign默认的 @QueryMap注解与Spring不兼容，因为它缺少value属性。
     * desc Spring Cloud OpenFeign提供等效的@SpringQueryMap注解，用于将POJO或Map参数注解为查询参数映射。
     * @param condition 入参
     * @param headers headers
     * @return 卡号
     */
    @GetMapping(value = "/card")
    CardVo getCard(@SpringQueryMap CardDto condition, @RequestHeader HttpHeaders headers);

    /**
     * 创建卡号
     * @param condition 入参
     * @param token token
     * @return 卡号
     */
    @PostMapping(value = "/card", headers = {"Content-Type=application/json;charset=UTF-8", "App-Secret=${app.secret}", "Authorization={Token}"})
    CardVo createCard(@RequestBody CardDto condition, @RequestHeader("Token") String token);

}
