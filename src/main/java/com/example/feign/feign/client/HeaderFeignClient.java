package com.example.feign.feign.client;

import com.example.feign.feign.config.FeignConfiguration;
import com.example.feign.feign.dto.CardDto;
import com.example.feign.feign.vo.CardVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author LiuDong
 * desc FeignClient设置Header的多种方式
 * See feign.SynchronousMethodHandler#invoke(java.lang.Object[])
 */
@FeignClient(url = "${virtual.api.url}", name = "header", configuration = FeignConfiguration.class)
public interface HeaderFeignClient {

    /**
     * 创建卡号
     * @param condition 入参
     * @return 卡号
     */
    @PostMapping(value = "/card")
    CardVo createCard(@RequestBody CardDto condition);

}
