package com.example.feign.feign.client;

import com.example.feign.feign.config.FooConfiguration;
import com.example.feign.feign.dto.CardDto;
import com.example.feign.feign.vo.CardVo;
import feign.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;

/**
 * @author LiuDong
 * desc FeignClient设置Header的多种方式
 * See feign.SynchronousMethodHandler#invoke(java.lang.Object[])
 */
@FeignClient(url = "${virtual.api.url}", name = "headers", configuration = FooConfiguration.class)
public interface HeadersFeignClient {

    /**
     * 获取卡号
     * @param condition 入参
     * @param headers   headers
     * @return 卡号
     */
    @RequestLine("POST /card")
    CardVo createCard(@QueryMap CardDto condition, @HeaderMap HttpHeaders headers);

    /**
     * 创建卡号
     * @param condition 入参
     * @param token     token
     * @return 卡号
     */
    @RequestLine("POST /card/batch")
    @Headers({"Content-Type: application/json;charset=UTF-8", "Accept: application/json", "Authorization: {token}"})
    CardVo batchCreateCard(@QueryMap CardDto condition, @Param("token") String token);

}
