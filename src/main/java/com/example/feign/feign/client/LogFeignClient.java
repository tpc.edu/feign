package com.example.feign.feign.client;

import com.example.feign.feign.config.LogFeignConfiguration;
import com.example.feign.feign.dto.CardDto;
import com.example.feign.feign.dto.Dto;
import com.example.feign.feign.vo.CardVo;
import com.example.feign.feign.vo.WeatherVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @date 20210322
 * @author LiuDong
 */
@FeignClient(url = "${weather.api.url}", name = "logFeignClient", configuration = LogFeignConfiguration.class)
public interface LogFeignClient {

    /**
     * 创建卡号
     * @param condition 入参
     * @return 卡号
     */
    @PostMapping(value = "/card")
    CardVo createCard(@RequestBody CardDto condition);

    /**
     * 查询指定城市天气状态
     * @param dto  AppId,AppSecret,Version,CityId
     * @return 天气状况
     */
    @PostMapping("/api")
    WeatherVo getEngineMessage(@SpringQueryMap Dto dto);

}
