package com.example.feign.feign.client;

import com.example.feign.feign.vo.WeatherVo;
import feign.ReflectiveFeign;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author LiuDong
 * desc FeignClientsRegistrar#registerBeanDefinitions, FeignClientsRegistra#registerFeignClientsr
 * desc FeignClientFactoryBean#getTarget() DefaultTargeter, HystrixTargeter
 * @see ReflectiveFeign#newInstance(feign.Target)
 * desc ReflectiveFeign.FeignInvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
 * desc SynchronousMethodHandler#invoke(java.lang.Object[]), SynchronousMethodHandler#executeAndDecode
 */
@FeignClient(url = "${weather.api.url}", name = "weather")
public interface WeatherFeignClient {

    /**
     * 查询指定城市天气状态
     * @param appId     AppId
     * @param appSecret AppSecret
     * @param cityId    CityId
     * @return 天气状况
     */
    @GetMapping("/api?version=v6&appid={appId}&appsecret={appSecret}")
    WeatherVo getEngineMessage(@PathVariable("appId") String appId, @PathVariable("appSecret") String appSecret, @RequestParam(name = "cityid", required = false) String cityId);

}
