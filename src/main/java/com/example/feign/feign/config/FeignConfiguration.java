package com.example.feign.feign.config;

import com.example.feign.feign.config.interceptor.FeignInterceptor;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;

/**
 * @author LiuDong
 * desc: 配置@Component 或 @Service 或 @Configuration 则会交给spring管理, 即可实现全局配置, 从而在所有FeignClient上起作用(即使该FeignClient上没有配置configuration属性)
 */
public class FeignConfiguration {

    @Bean
    public RequestInterceptor feignInterceptor() {
        return new FeignInterceptor();
    }

}
