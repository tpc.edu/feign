package com.example.feign.feign.config;

import com.example.feign.feign.config.client.LogClient;
import com.example.feign.feign.config.codec.RawErrorDecoder;
import com.example.feign.feign.config.interceptor.FeignInterceptor;
import feign.Client;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

/**
 * @date 20210322
 * @author LiuDong
 */
public class LogFeignConfiguration {

    @Bean
    public Client feignClient() {
        return new LogClient(null, null);
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new RawErrorDecoder();
    }

    @Bean
    public RequestInterceptor feignInterceptor() {
        return new FeignInterceptor();
    }

}
