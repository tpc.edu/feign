package com.example.feign.feign.config.client;

import feign.Response;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.util.StreamUtils;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

/**
 * @author LiuDong
 * @date 2021-03-21
 */
final class BufferingFeignClientResponse implements Closeable {

    private Response response;

    @Nullable
    private byte[] body;

    public BufferingFeignClientResponse(Response response) {
        this.response = response;
    }

    public HttpStatus getStatusCode() {
        return HttpStatus.valueOf(this.response.status());
    }

    public Response getResponse() {
        return this.response;
    }

    public int getRawStatusCode() {
        return this.response.status();
    }

    public String getStatusText() {
        return HttpStatus.valueOf(this.response.status()).name();
    }

    public Map<String, Collection<String>> getHeaders() {
        return this.response.headers();
    }

    public InputStream getBody() throws IOException {
        if (this.body == null) {
            this.body = StreamUtils.copyToByteArray(this.response.body().asInputStream());
        }
        return new ByteArrayInputStream(this.body);
    }

    @Override
    public void close() {
        this.response.close();
    }

}

