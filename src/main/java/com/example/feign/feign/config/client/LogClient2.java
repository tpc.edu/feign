package com.example.feign.feign.config.client;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import feign.Client;
import feign.Request;
import feign.Response;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * @author liweigao
 * @date 2019/8/26 上午10:17
 */
@Slf4j
public class LogClient2 extends Client.Default {

    private static final String CONTENT_TYPE = "Content-Type";

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * Null parameters imply platform defaults.
     * @param sslContextFactory ssl
     * @param hostnameVerifier  hostname
     */
    public LogClient2(SSLSocketFactory sslContextFactory, HostnameVerifier hostnameVerifier) {
        super(sslContextFactory, hostnameVerifier);
    }

    @Override
    public Response execute(Request request, Request.Options options) throws IOException {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String errorMsg = null;

        BufferingFeignClientResponse bufferingFeignClientResponse = null;
        try {
            bufferingFeignClientResponse = new BufferingFeignClientResponse(super.execute(request, options));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            errorMsg = e.getMessage();
            throw e;
        } finally {
            stopWatch.stop();

            // request
            Map reqMap = null;
            byte[] body = request.body();
            Charset charset = Objects.isNull(request.charset()) ? Charset.defaultCharset() : request.charset();
            HttpHeaders httpHeaders = convert(request.headers());
            String reqStr = Strings.EMPTY;
            MediaType reqMediaType;
            if (Objects.nonNull(reqMediaType = httpHeaders.getContentType())) {
                if (reqMediaType.includes(MediaType.MULTIPART_FORM_DATA)) {
                    body = new byte[]{0};
                }
                if (Objects.nonNull(body)) {
                    reqStr = new String(body, charset);
                }
                if ((reqMediaType.includes(MediaType.APPLICATION_JSON_UTF8) || reqMediaType.includes(MediaType.APPLICATION_JSON))) {
                    //json format parameters
                    try {
                        reqMap = JSON.parseObject(reqStr);
                        reqStr = null;
                        //no care this exception
                    } catch (JSONException e) {
                    }
                }
            }

            //response
            Map respMap = null;
            String respStr = null;
            int resStatus;
            Collection<String> collection;
            if (Objects.nonNull(bufferingFeignClientResponse)) {
                if (Objects.nonNull(bufferingFeignClientResponse.getHeaders()) && !CollectionUtils.isEmpty(collection =
                        bufferingFeignClientResponse.getHeaders().get(CONTENT_TYPE))) {

                    StringBuilder resBody = new StringBuilder();
                    try (BufferedReader bufferedReader =
                                 new BufferedReader(new InputStreamReader(bufferingFeignClientResponse.getBody(),
                                         charset))) {
                        String line = bufferedReader.readLine();
                        while (line != null) {
                            resBody.append(line);
                            line = bufferedReader.readLine();
                        }
                    }
                    if (!collection.contains(MediaType.MULTIPART_FORM_DATA_VALUE)) {
                        respStr = resBody.toString();
                    }
                    if (collection.contains(MediaType.APPLICATION_JSON_VALUE) || collection.contains(MediaType.APPLICATION_JSON)) {
                        try {
                            respMap = JSON.parseObject(respStr);
                            respStr = null;
                            //no care this exception
                        } catch (JSONException e) {
                        }
                    }
                }
                resStatus = bufferingFeignClientResponse.getRawStatusCode();
            } else {
                resStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
                respStr = errorMsg;
            }

            RestLog restLog = RestLog.builder().costTime(stopWatch.getLastTaskTimeMillis()).headers(httpHeaders)
                    .method(request.method()).reqBody(reqStr).resJson(respMap).reqJson(reqMap).reqUrl(request.url())
                    .resBody(respStr).resStatus(resStatus).build();
            log.info("resLog: {}", restLog.toString());
            redisTemplate.opsForValue().set("resLog" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")), restLog.toString());
        }

        Response response = bufferingFeignClientResponse.getResponse().toBuilder()
                .body(bufferingFeignClientResponse.getBody(),
                        bufferingFeignClientResponse.getResponse().body().length()).build();
        bufferingFeignClientResponse.close();

        return response;
    }


    private HttpHeaders convert(Map<String, Collection<String>> headers) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (Objects.nonNull(headers)) {
            headers.forEach((k, v) -> {
                httpHeaders.set(k, convert(v));
            });
        }
        return httpHeaders;
    }

    private String convert(Collection<String> strings) {
        StringBuilder builder = new StringBuilder();

        strings.forEach(s -> {
            builder.append(s).append(",");
        });

        //去除末尾逗号
        if (builder.length() > 0) {
            builder.delete(builder.length() - 1, builder.length());
        }
        return builder.toString();
    }

    @Data
    @Builder
    @SuppressWarnings("rawtypes")
    private static class RestLog {
        private String reqUrl;
        private String method;
        private HttpHeaders headers;
        private String reqBody;
        private String resBody;
        private Map resJson;
        private Map reqJson;
        private long costTime;
        private int resStatus;
    }
}
