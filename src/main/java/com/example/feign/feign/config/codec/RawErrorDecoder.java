package com.example.feign.feign.config.codec;

import feign.Response;
import feign.RetryableException;
import feign.Util;
import feign.codec.ErrorDecoder;

import java.io.IOException;

/**
 * @author LiuDong
 * @description 保留 feign 服务异常信息
 */
public class RawErrorDecoder extends ErrorDecoder.Default {

    /**
     * 参考 feign.FeignException#errorStatus(java.lang.String, feign.Response) 基于message或status的不同返回不同异常
     * @param methodKey feignClient.method
     * @param response response
     * @return feign调用真实异常
     */
    @Override
    public Exception decode(String methodKey, Response response) {
        Exception exception = super.decode(methodKey, response);

        if (exception instanceof RetryableException) {
            return exception;
        }

        try {
            if (response.body() != null) {
                // 获取feign调用的响应体, 后期可以转换成自定义的实体, 然后返回对应异常
                String message = Util.toString(response.body().asReader(Util.UTF_8));
                return new RuntimeException(message);
            }
        } catch (IOException ignored) {
        }

        return exception;
    }

}

