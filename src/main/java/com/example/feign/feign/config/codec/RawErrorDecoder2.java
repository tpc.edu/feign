package com.example.feign.feign.config.codec;

import com.alibaba.fastjson.JSONObject;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;

/**
 * @author LiuDong
 * @description feign 服务异常不进入熔断
 */
public class RawErrorDecoder2 implements ErrorDecoder {

    /**
     * 参考 feign.FeignException#errorStatus(java.lang.String, feign.Response) 基于message或status的不同返回不同异常
     * @param methodKey feignClient.method
     * @param response response
     * @return feign调用真实异常
     */
    @Override
    public Exception decode(String methodKey, Response response) {

        String message = null;
        try {
            if (response.body() != null) {
                message = Util.toString(response.body().asReader(Util.UTF_8));
                JSONObject json = JSONObject.parseObject(message);
                return new RuntimeException(json.getString("message"));
            }
        } catch (Exception ignored) {
        }

        return new RuntimeException(message);
    }

}

