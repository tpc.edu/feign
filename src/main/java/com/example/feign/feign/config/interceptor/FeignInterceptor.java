package com.example.feign.feign.config.interceptor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONPObject;
import feign.Request;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

/**
 * @author LiuDong
 * desc: 配置@Component 或 @Service 或 @Configuration 则会交给spring管理, 即可实现全局配置, 从而在所有FeignClient上起作用(即使该FeignClient上没有配置configuration属性)
 * See feign.auth.BasicAuthRequestInterceptor, org.springframework.cloud.openfeign.security.OAuth2FeignRequestInterceptor
 */
public class FeignInterceptor implements RequestInterceptor {

    /**
     * The HTTP {@code Request-Id} header field name.
     */
    public static final String REQUEST_ID = "Request-Id";

    @Override
    public void apply(RequestTemplate template) {
        template.header(REQUEST_ID, "Request-Id:uuid");
        template.header(HttpHeaders.AUTHORIZATION, "tokenVal");
    }

//    @Override
//    public void apply(RequestTemplate template) {
//        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        assert attributes != null;
//        HttpServletRequest request = attributes.getRequest();
//        Enumeration<String> headerNames = request.getHeaderNames();
//        if (headerNames != null) {
//            while (headerNames.hasMoreElements()) {
//                String name = headerNames.nextElement();
//                String values = request.getHeader(name);
//                template.header(name, values);
//            }
//        }
//    }

//    @Override
//    public void apply(RequestTemplate template) {
//        // 如果是get请求
//        if (template.method().equals(Request.HttpMethod.GET.name())) {
//            //获取到get请求的参数
//            Map<String, Collection<String>> queries = template.queries();
//        }
//        //如果是Post请求
//        if (template.method().equals(Request.HttpMethod.POST.name())) {
//            //获得请求body
//            String body = template.requestBody().asString();
//            JSONPObject request = JSON.parseObject(body, JSONPObject.class);
//        }
//
//        //Do what you want... 例如生成接口签名
//        String sign = "根据请求参数生成的签名";
//
//        //放入url？之后
//        template.query("sign", sign);
//        //放入请求body中
//        String newBody = "原有body" + sign;
//        template.body(Request.Body.encoded(newBody.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8));
//    }
}
