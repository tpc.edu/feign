package com.example.feign.feign.dto;

import com.example.feign.retful.dto.WeatherDto;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author LiuDong
 * 此处省略Swagger代码
 */
@Data
@Builder
public class Dto implements Serializable {
    /** 用户AppId */
    private String appid = "91555498";
    /** 用户AppSecret */
    private String appsecret = "UCn6CdSR";
    /** 接口版本标识(固定值: v6 每个接口的version值都不一样) */
    private String version = "v6";
    /** 请参考城市ID列表 */
    private String cityid = "101020100";

    public static Dto turnDto(WeatherDto weatherDto) {
        if (weatherDto == null) {
            return null;
        }
        return builder()
                .appid(weatherDto.getAppId())
                .appsecret(weatherDto.getAppSecret())
                .version(weatherDto.getVersion())
                .cityid(weatherDto.getCityId())
                .build();
    }
}
