package com.example.feign.feign.oauth2;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LiuDong
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "app.security.urls")
public class AppSecurityUrlsProperties {

    private List<String> resource = new ArrayList<>();

    private List<String> ignored = new ArrayList<>();
}
