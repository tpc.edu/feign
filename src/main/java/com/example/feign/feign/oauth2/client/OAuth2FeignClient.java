package com.example.feign.feign.oauth2.client;

import com.example.feign.feign.oauth2.config.OAuth2FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @date 20210322
 * @author LiuDong
 */
@FeignClient(url = "${oauth2.api.url}", name = "oauth2FeignClient", configuration = OAuth2FeignConfiguration.class)
public interface OAuth2FeignClient {

    /**
     * 查询成员信息
     * @return 信息
     */
    @PostMapping("/member/info")
    String getMemberInfo();

}
