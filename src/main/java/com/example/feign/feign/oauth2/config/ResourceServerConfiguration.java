package com.example.feign.feign.oauth2.config;

import com.example.feign.feign.oauth2.AppSecurityUrlsProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * @author itw_liudong02
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    private AppSecurityUrlsProperties appSecurityUrlsProperties;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.requestMatchers().antMatchers(appSecurityUrlsProperties.getResource().toArray(new String[0])).and().authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers(appSecurityUrlsProperties.getIgnored().toArray(new String[0])).permitAll().anyRequest().authenticated();
    }
}

