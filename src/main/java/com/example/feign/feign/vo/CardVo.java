package com.example.feign.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author LiuDong
 * 此处省略Swagger代码
 */
@Data
@Accessors(chain = true)
public class CardVo implements Serializable {
    /** 虚拟卡类型 */
    private String cardType;
    /** 虚拟卡账号 */
    private String cardNo;
    /** 虚拟卡密码 */
    private String cardSecret;
    /** 虚拟卡面值 */
    private String faceValue;
}
