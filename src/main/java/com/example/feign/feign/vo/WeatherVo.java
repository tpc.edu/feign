package com.example.feign.feign.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author LiuDong
 * 此处省略Swagger代码
 */
@Data
@Accessors(chain = true)
public class WeatherVo implements Serializable {
    /** 城市ID */
    private String cityid;
    /** 当前日期 */
    private String date;
    /** 当前星期 */
    private String week;
    /** 气象台更新时间 */
    private String update_time;
    /** 城市名称 */
    private String city;
    /** 城市英文名称 */
    private String cityEn;
    /** 国家名称 */
    private String country;
    /** 国家英文名称 */
    private String countryEn;
    /** 天气情况 */
    private String wea;
    /** 天气对应图标[固定9种类型(您也可以根据wea字段自己处理):xue、lei、shachen、wu、bingbao、yun、yu、yin、qing] */
    private String wea_img;
    /** 实时温度 */
    private String tem;
    /** 高温 */
    private String tem1;
    /** 低温 */
    private String tem2;
    /** 风向 */
    private String win;
    /** 风力等级 */
    private String win_speed;
    /** 风速 */
    private String win_meter;
    /** 湿度 */
    private String humidity;
    /** 能见度 */
    private String visibility;
    /** 气压hPa */
    private String pressure;
    /** 空气质量 */
    private String air;
    /** 空气质量等级 */
    private String air_level;
    /** 空气质量描述 */
    private String air_tips;
}
