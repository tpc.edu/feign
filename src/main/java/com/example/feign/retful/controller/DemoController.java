package com.example.feign.retful.controller;

import com.example.feign.feign.client.WeatherFeignClient;
import com.example.feign.feign.vo.WeatherVo;
import com.example.feign.retful.dto.WeatherDto;
import com.example.feign.retful.response.ApiResponse;
import com.example.feign.retful.response.ApiSuccessEnum;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LiuDong
 */
@RestController
@RequestMapping("/demo/api")
public class DemoController {

    @Resource
    private WeatherFeignClient weatherFeignClient;

    @PostMapping
    public ApiResponse<WeatherVo> getWeatherMessage(@RequestBody WeatherDto dto) {
        WeatherVo vo = weatherFeignClient.getEngineMessage(dto.getAppId(), dto.getAppSecret(), dto.getCityId());
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

}
