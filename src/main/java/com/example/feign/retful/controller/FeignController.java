package com.example.feign.retful.controller;

import com.example.feign.feign.client.HeadFeignClient;
import com.example.feign.feign.client.HeaderFeignClient;
import com.example.feign.feign.client.HeadersFeignClient;
import com.example.feign.feign.client.LogFeignClient;
import com.example.feign.feign.dto.CardDto;
import com.example.feign.feign.dto.Dto;
import com.example.feign.feign.oauth2.client.OAuth2FeignClient;
import com.example.feign.feign.vo.CardVo;
import com.example.feign.feign.vo.WeatherVo;
import com.example.feign.retful.dto.WeatherDto;
import com.example.feign.retful.response.ApiResponse;
import com.example.feign.retful.response.ApiSuccessEnum;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author LiuDong
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class FeignController {

    @Resource
    private LogFeignClient logFeignClient;
    @Resource
    private HeadFeignClient headFeignClient;
    @Resource
    private HeaderFeignClient headerFeignClient;
    @Resource
    private HeadersFeignClient headersFeignClient;
    @Resource
    private OAuth2FeignClient oAuth2FeignClient;

    @GetMapping
    public ApiResponse<CardVo> getCard(@RequestBody CardDto dto) {
        Map<String, List<String>> headerMap = Maps.newHashMap();
        headerMap.put(HttpHeaders.CONTENT_TYPE, Lists.newArrayList(MediaType.APPLICATION_FORM_URLENCODED_VALUE));
        headerMap.put(HttpHeaders.AUTHORIZATION, Lists.newArrayList("tokenVal"));
        CardVo vo = headFeignClient.getCard(dto, new HttpHeaders(CollectionUtils.toMultiValueMap(headerMap)));
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @PostMapping
    public ApiResponse<CardVo> saveCard(@RequestBody CardDto dto) {
        CardVo vo = headFeignClient.createCard(dto, "tokenVal");
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @PostMapping("/sole")
    public ApiResponse<CardVo> soleSaveCard(@RequestBody CardDto dto) {
        CardVo vo = headerFeignClient.createCard(dto);
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @GetMapping("/batch")
    public ApiResponse<CardVo> batchGetCard(@RequestBody CardDto dto) {
        Map<String, List<String>> headerMap = Maps.newHashMap();
        headerMap.put(HttpHeaders.CONTENT_TYPE, Lists.newArrayList(MediaType.APPLICATION_JSON_VALUE));
        headerMap.put(HttpHeaders.AUTHORIZATION, Lists.newArrayList("tokenVal"));
        CardVo vo = headersFeignClient.createCard(dto, new HttpHeaders(CollectionUtils.toMultiValueMap(headerMap)));
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @PostMapping("/batch")
    public ApiResponse<CardVo> batchSaveCard(@RequestBody CardDto dto) {
        CardVo vo = headersFeignClient.batchCreateCard(dto, "tokenVal");
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @PostMapping("/log")
    public ApiResponse<CardVo> logSaveCard(@RequestBody CardDto dto) {
        CardVo vo = logFeignClient.createCard(dto);
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @PostMapping("/log/weather")
    public ApiResponse<WeatherVo> getWeatherMessage(@RequestBody WeatherDto weatherDto) {
        WeatherVo vo = logFeignClient.getEngineMessage(Dto.turnDto(weatherDto));
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @PostMapping("/oauth2")
    public ApiResponse<String> getMemberInfo() {
        String vo = oAuth2FeignClient.getMemberInfo();
        log.info("vo:{}", vo);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

}
