package com.example.feign.retful.controller;

import com.example.feign.feign.client.FileFeignClient;
import com.example.feign.feign.dto.CardDto;
import com.example.feign.feign.vo.CardVo;
import com.example.feign.retful.response.ApiResponse;
import com.example.feign.retful.response.ApiSuccessEnum;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author LiuDong
 */
@RestController
@RequestMapping("/api/file")
public class FileController {

    @Resource
    private FileFeignClient fileFeignClient;

    @PostMapping("/card")
    public CardVo create(@RequestBody CardDto condition) {
        return new CardVo().setCardType(condition.getCardType()).setFaceValue(condition.getFaceValue());
    }

    @PostMapping("upload")
    public CardVo upload(@RequestParam MultipartFile file, @RequestParam("name") String name) {
        return new CardVo().setCardType(file.getOriginalFilename()).setFaceValue(String.valueOf(file.getSize())).setCardNo(name);
    }

    @PostMapping("/card/demo")
    public ApiResponse<CardVo> createDemo(@RequestBody CardDto condition) {
        CardVo vo = fileFeignClient.create(condition);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

    @PostMapping("/upload/demo")
    public ApiResponse<CardVo> uploadDemo(@RequestParam MultipartFile file, @RequestParam("name") String name) {
        CardVo vo = fileFeignClient.upload(file, name);
        return ApiResponse.success(ApiSuccessEnum.ONE, vo);
    }

}
