package com.example.feign.retful.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author LiuDong
 * 此处省略Swagger代码
 */
@Data
@Accessors(chain = true)
public class WeatherDto implements Serializable {
    /** 用户AppId */
    private String appId = "91555498";
    /** 用户AppSecret */
    private String appSecret = "UCn6CdSR";
    /** 接口版本标识(固定值: v6 每个接口的version值都不一样) */
    private String version = "v6";
    /** 请参考城市ID列表 */
    private String cityId = "101020100";
}
