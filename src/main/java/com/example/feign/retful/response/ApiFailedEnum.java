package com.example.feign.retful.response;

import lombok.Getter;

/**
 * @author LiuDong
 */
@Getter
public enum ApiFailedEnum implements IFailedEnum {
    /** 失败编码, 失败说明 */
    ONE("F00001", "查询失败!");

    private final String code;
    private final String message;

    ApiFailedEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ApiFailedEnum getEnumByCode(String code) {
        for(ApiFailedEnum failedEnum : ApiFailedEnum.values()) {
            if (failedEnum.getCode().equals(code)) {
                return failedEnum;
            }
        }
        return null;
    }
}
