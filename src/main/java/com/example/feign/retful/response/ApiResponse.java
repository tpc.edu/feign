package com.example.feign.retful.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author LiuDong
 * 此处省略Swagger代码
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ApiResponse<T> implements Serializable {
    /** 响应状态码 */
    private String code;
    /** 响应消息体 */
    private String message;
    /** 响应数据体 */
    private T data;

    public static <T> ApiResponse<T> failure(IFailedEnum failedEnum, T data) {
        return new ApiResponse<>(failedEnum.getCode(), failedEnum.getMessage(), data);
    }

    public static <T> ApiResponse<T> success(ISuccessEnum successEnum, T data) {
        return new ApiResponse<>(successEnum.getCode(), successEnum.getMessage(), data);
    }

    private ApiResponse(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
