package com.example.feign.retful.response;

import lombok.Getter;

/**
 * @author LiuDong
 */
@Getter
public enum ApiSuccessEnum implements ISuccessEnum {
    /** 成功编码, 成功说明 */
    ONE("S00001", "查询成功!");

    private final String code;
    private final String message;

    ApiSuccessEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ApiSuccessEnum getEnumByCode(String code) {
        for(ApiSuccessEnum successEnum : ApiSuccessEnum.values()) {
            if (successEnum.getCode().equals(code)) {
                return successEnum;
            }
        }
        return null;
    }
}
