package com.example.feign.retful.response;

/**
 * @author LiuDong 
 */
public interface IFailedEnum {
    /**
     * 获取失败状态码
     * @return 失败状态码
     */
    String getCode();

    /**
     * 获取失败描述
     * @return 失败描述
     */
    String getMessage();
}
