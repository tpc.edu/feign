package com.example.feign.retful.response;

/**
 * @author LiuDong 
 */
public interface ISuccessEnum {
    /**
     * 获取成功状态码
     * @return 成功状态码
     */
    String getCode();

    /**
     * 获取成功描述
     * @return 成功描述
     */
    String getMessage();
}
