package com.example.feign.client;

import feign.Param;
import feign.RequestLine;

/**
 * @author LiuDong
 */
public interface HelloFeignClient {

    /**
     * 查询指定城市天气状态
     * @param appId     AppId
     * @param appSecret AppSecret
     * @return 天气状况
     */
    @RequestLine("GET /api?version=v6&appid={appId}&appsecret={appSecret}")
    String getEngineMessage(@Param("appId") String appId, @Param("appSecret") String appSecret);

}
