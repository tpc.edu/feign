package com.example.feign.config;

import feign.Client;
import feign.Request;
import feign.Response;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * @author LiuDong
 */
public class HelloClient implements Client {

    public Response execute(Request request, Request.Options options) throws IOException {
        System.out.println("execute request method=" + request.method());

        System.out.println("execute request headers");
        Map<String, Collection<String>> headers = request.headers();
        for (Map.Entry<String, Collection<String>> entry : headers.entrySet()) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < entry.getValue().size(); i++) {
                if (stringBuilder.length() > 0) {
                    stringBuilder.append(",");
                }
                stringBuilder.append(entry.getValue());
            }
            System.out.println(entry.getKey() + ":" + stringBuilder.toString());
        }

        byte[] body = request.body();
        if (body != null) {
            System.out.println("execute request body=" + new String(body));
        }
        // 使用 Feign 默认的客户端请求
        return new Client.Default(null, null).execute(request, options);
    }

}
