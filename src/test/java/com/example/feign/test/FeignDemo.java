package com.example.feign.test;

import com.example.feign.client.HelloFeignClient;
import com.example.feign.config.HelloClient;
import com.example.feign.retful.dto.WeatherDto;
import feign.Feign;

/**
 * @author LiuDong
 * reference https://www.cnblogs.com/li3807/p/8890622.html, https://segmentfault.com/a/1190000018914017
 */
public class FeignDemo {

    /**
     * 由于我在`feign.codec.StringDecoder#decode(feign.Response, java.lang.reflect.Type)`这个方法里`return Util.toString(body.asReader(Util.UTF_8));`这行上一行打了注释,导致toString() is called on the response
     * 导致idea调试时报错: feign.FeignException: stream is closed reading GET https://tianqiapi.com/api?version=v6&appid=91555498&appsecret=UCn6CdSR
     *
     * 错误原因分析: https://stackoverflow.com/questions/62929527/how-to-get-rid-of-stream-is-closed-error-with-feignerrordecoder
     *   By the way, be careful with debugging this kind of code. Modern IDEs (e.g. IntelliJ IDEA) can invoke toString() on all objects in the scope while reaching a breakpoint by default,
     *   so the same problem can occur because of it too. Here, you can safely put the breakpoint only after MAPPER.readValue line.
     */
    public static void main(String[] args) {
        HelloFeignClient helloFeignClient = Feign.builder()
                .client(new HelloClient())
                .target(HelloFeignClient.class, "https://tianqiapi.com");
        String vo = helloFeignClient.getEngineMessage(new WeatherDto().getAppId(), new WeatherDto().getAppSecret());
        System.out.println("WeatherVo: " + vo);
    }

}
